# fish_escapes
A GLOBAL ASSESSMENT OF ECOLOGICAL RISKS ASSOCIATED WITH FARMED FISH ESCAPES

Javier Atalah and Pablo Sanchez-Jerez

Data and scripts used for a global assessment and visualisation of the invasive, genetic and pathogenic impacts risk associated with escapes of farmed fish into Marine Ecoregions of the World.

Published in Global Ecology and Conservation

https://doi.org/10.1016/j.gecco.2019.e00842
